# Symfony Console Run

## Package Info
Name: `symfonyconsolerun`
Alias to run: `sc`

## Scope

This tool will find your bin/console and run it. 
Also it will check if you have permissions to run it or give you option to make it executable.

## Important Notes

This is build for Unix Systems for now, but feel free to use the libs of python and build it for any system you need.

## Reqirements

* BASH 
* Python (=>2.6)

## Installation

On Debian based systems:

	From `packages` download the latest version and run `sudo dpkg -i theDownloadedPackage.deb`

## Final Goal

Compile it as .deb package with reqired dependencies (e.g. python, bash etc...), to be able to install it on *unix

## Change Log
