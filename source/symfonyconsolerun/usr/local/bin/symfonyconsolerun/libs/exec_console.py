import os, fnmatch

CONSOLE_FILENAME = "console"
WORKING_DIR = os.getcwd()

def find(pattern, path):
    result = None
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result = os.path.join(root, name)
    return result

FOUND=False

SEARCH_PATH = WORKING_DIR
while True:
##    print("Searching path ...", SEARCH_PATH)
    RESULT = find(CONSOLE_FILENAME, SEARCH_PATH)
    if not RESULT:
##        print("Not found.. cutting dir...")
        SEARCH_PATH = SEARCH_PATH.rsplit('/', 1)[0]
    else:
        break;

print(RESULT)
