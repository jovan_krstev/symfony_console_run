#!/bin/bash 

echo "### Creating deb package"

### PATHS
DIR_LOCATION="usr/local/bin/"
DIR_SOURCE="source/"
P_NAME="symfonyconsolerun"
DIR_PACKAGES="packages/"

echo -n "Enter version number and press [ENTER] (ex. 1.0-1): "
read version

echo "$DIR_SOURCE$DIR_LOCATION"

dpkg-deb --build $DIR_SOURCE$P_NAME/

mv $DIR_SOURCE$P_NAME.deb $DIR_PACKAGES/$P_NAME-$version.deb
